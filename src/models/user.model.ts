import { Document, Model, model, Schema } from "mongoose";
import { LoginType, PersonalityType, LikeFactor } from "polyteam-shared";
import { MBTIPoints } from "../config/mbti";
import { Map } from "utils/type.utils";

export interface ILikeUser {
  email: string;
  factor: LikeFactor;
}

interface ILikedUsers {
  [course: string]: ILikeUser[];
}

export class IUser {
  email: string = undefined;
  fname: string = undefined;
  lname: string = undefined;
  profilepic: string = undefined;
  password: string = undefined;
  type: LoginType = undefined;
  jwt: string = undefined;
  mbtiPoints?: MBTIPoints = undefined;
  mbtiType?: PersonalityType = undefined;
  created?: Date = undefined;
  connected?: boolean = undefined;
  likedUsers: ILikedUsers = undefined;
  codes: Map<string> = undefined;
}

export interface IUserDocument extends IUser, Document {}

export let UserSchema: Schema = new Schema({
  email: {
    type: Schema.Types.String,
    unique: true,
    required: true,
  },
  fname: {
    type: Schema.Types.String,
    required: true,
  },
  lname: {
    type: Schema.Types.String,
    required: true,
  },
  profilepic: {
    type: Schema.Types.String,
  },
  password: {
    type: Schema.Types.String,
    required: true,
  },
  type: {
    type: Schema.Types.String,
    required: true,
  },
  jwt: {
    type: Schema.Types.String,
    required: true,
  },
  mbtiPoints: {
    type: Schema.Types.Mixed,
  },
  mbtiType: {
    type: Schema.Types.Mixed,
  },
  created: {
    type: Schema.Types.String,
    default: Date.now(),
  },
  connected: {
    type: Schema.Types.Boolean,
    default: true,
  },
  likedUsers: {
    type: Schema.Types.Mixed,
    default: {},
  },
  codes: {
    type: Schema.Types.Mixed,
    default: {},
  },
});

export const User: Model<IUserDocument> = model<IUserDocument>(
  "User",
  UserSchema
);
