import { Document, Model, model, Schema } from "mongoose";

export class ICode {
  code: string = undefined;
  course: string = undefined;
  creator: string = undefined;
  members: string[] = undefined;
}

export interface ICodeDocument extends ICode, Document {}

export let CodeSchema: Schema = new Schema({
  code: {
    type: Schema.Types.String,
    unique: true,
    required: true,
  },
  course: {
    type: Schema.Types.String,
    required: true,
  },
  creator: {
    type: Schema.Types.String,
    required: true,
  },
  members: {
    type: [String],
    required: true,
  },
});

export const Code: Model<ICodeDocument> = model("Code", CodeSchema);
