import { Document, Model, model, Schema } from "mongoose";
import {
  IUserGroupSettings,
  ICourseTeam,
  ICreateAccount,
} from "polyteam-shared";
import { BooleanUtils } from "../utils/boolean.utils";

export class ICourse {
  courseId: string = undefined;
  users: string[] = undefined;
  teacher: string = undefined;
  settings: IUserGroupSettings = undefined;
  teams: ICourseTeam[] = undefined;

  constructor(
    id: string,
    users: ICreateAccount[],
    teacher: string,
    settings: IUserGroupSettings
  ) {
    this.courseId = id;
    this.users = users.map(u => u.email);
    this.teacher = teacher;
    this.settings = settings;

    const ready = BooleanUtils.inRange(1, this.settings.nbUsers);
    const validated: boolean = false;
    this.teams = users.map(({ email, fname, lname }) => ({
      users: [{ email, fname, lname, validated }],
      ready,
    }));
  }
}

export interface ICourseDocument extends ICourse, Document {}

export let CourseSchema: Schema = new Schema({
  courseId: {
    type: Schema.Types.String,
    unique: true,
    required: true,
  },
  users: {
    type: [String],
    required: true,
  },
  teacher: {
    type: String,
    required: true,
  },
  settings: {
    type: Object,
    required: true,
  },
  teams: {
    type: [Object],
    required: true,
  },
});

export const Course: Model<ICourseDocument> = model<ICourseDocument>(
  "Course",
  CourseSchema
);
