import { Document, Model, model, Schema } from "mongoose";

export class IBackup {
  key: string;
  backup: any;
}

export interface IBackupDocument extends IBackup, Document {}

export let BackupSchema: Schema = new Schema({
  key: {
    type: Schema.Types.String,
    unique: true,
    required: true,
  },
  backup: {
    type: Schema.Types.Mixed,
    default: {},
  },
});

export const Backup: Model<IBackupDocument> = model("Backup", BackupSchema);
