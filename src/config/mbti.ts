import { MBTIForm } from "polyteam-shared";
import { StrictMap } from "../utils/type.utils";

const MBTI_PERSONALITY = ["E", "I", "S", "N", "T", "F", "J", "P"] as const;

type MBTIPersonality = typeof MBTI_PERSONALITY[number];

type MBTIPoints = StrictMap<MBTIPersonality, number>;

interface MBTIValidator {
  id: number;
  v1: MBTIPersonality;
  v2: MBTIPersonality;
}

interface MBTIValidationPart {
  id: number;
  validators: MBTIValidator[];
}

type MBTIValidation = MBTIValidationPart[];

const MBTI_FORM: MBTIForm = [
  {
    id: 1,
    title:
      "Quelle réponse correspond le mieux à la façon dont vous vous sentez ou agissez habituellement?",
    questions: [
      {
        id: 1,
        question:
          "Quand vous allez quelque part pour la journée, préférez-vous",
        a1: "planifier ce que vous ferez et quand",
        a2: "sans planification",
      },
      {
        id: 2,
        question: "Vous considérez-vous comme",
        a1: "une personne spontanée",
        a2: "une personne organisée",
      },
      {
        id: 3,
        question: "Si vous étiez professeur, préférez-vous enseigner",
        a1: "cours de travail pratique",
        a2: "cours de théorie",
      },
      {
        id: 4,
        question: " Etes-vous habituellement",
        a1: "Un bon mixe ",
        a2: "plutôt calme et réservé",
      },
      {
        id: 5,
        question: "Vous vous entendez habituellement le plus avec des ",
        a1: "gens qui ont beaucoup d'imagination",
        a2: "personnes réalistes",
      },
      {
        id: 6,
        question: "Vous laissez habituellement ",
        a1: "votre cœur domine votre tête",
        a2: "votre tête domine votre coeur",
      },
      {
        id: 7,
        question: "Préférez-vous faire beaucoup de choses",
        a1: "par impulsion du moment",
        a2: "selon les plans",
      },
      {
        id: 8,
        question: "Etes-vous",
        a1: "facile à cerner",
        a2: "difficile à cerner",
      },
      {
        id: 9,
        question: "Est-ce que suivre un planning ",
        a1: "ça vous intéresse",
        a2: "ça ne vous intéresse pas",
      },
      {
        id: 10,
        question: "Quand vous avez un travail spécial à faire, vous aimez ",
        a1: "l'organiser soigneusement avant de commencer",
        a2: "découvrir ce qui est nécessaire au fur et à mesure",
      },
      {
        id: 11,
        question: "Dans la plupart des cas, vous préférez",
        a1: "suivre un plan",
        a2: "se laisser entraîner par le courant",
      },
      {
        id: 12,
        question: "La plupart des gens jugent que vous êtes",
        a1: "une personne renfermée",
        a2: "une personne très ouverte",
      },
      {
        id: 13,
        question: "Vous préférez être considéré",
        a1: "une personne pratique",
        a2: "une personne ingénieuse",
      },
      {
        id: 14,
        question: "Dans un groupe de personnes vous",
        a1: "commencez souvent par présenter les autres",
        a2: "vous présentez souvent en premier",
      },
      {
        id: 15,
        question: "Votre ami est plutôt une personne qui",
        a1: "propose toujours de nouvelles idées",
        a2: "a les pieds sur terre",
      },
      {
        id: 16,
        question: "Vous avez tendance à",
        a1: "valoriser les sentiments plus que la logique",
        a2: "valoriser la logique plus que les sentiments",
      },
      {
        id: 17,
        question: "Vous préférez",
        a1: "attendre et voir ce qui se passe puis faire un plan",
        a2: "faire un plan à l'avance",
      },
      {
        id: 18,
        question: "Vous avez tendance à passer beaucoup de temps",
        a1: "tout seul",
        a2: "avec les autres",
      },
      {
        id: 19,
        question: "Vous trouvez qu'être autour de beaucoup de personnes",
        a1: "vous donnes de l'energie",
        a2: "est épuisant",
      },
      {
        id: 20,
        question: "Vous préférez",
        a1: "tout organiser :les dates,les sorties,etc.",
        a2:
          "être libre de faire ce qui vous semble amusant et quand vous voulez",
      },
      {
        id: 21,
        question: "Pour planifier un voyage, vous préférez",
        a1: "suivre vos envies chaque jour",
        a2: "savoir à l'avance ce que vous allez faire pendant votre séjour",
      },
      {
        id: 22,
        question: "Lors d'une soirée entre amis, vous",
        a1: "vous ennuyez",
        a2: "vous amusez comme d'habitude",
      },
      {
        id: 23,
        question: "En général êtes vous quelqu'un",
        a1: "qui aime bien être entouré",
        a2: "qui préférez rester seul",
      },
      {
        id: 24,
        question: "Vous êtes plus attiré par",
        a1: "une personne avec un esprit vif et brillant",
        a2: "une personne qui fait preuve de bon sens",
      },
      {
        id: 25,
        question: "Dans votre travail quotidien, vous voulez",
        a1: "travailler sous pression",
        a2: "planifier généralement votre travail sans pression",
      },
      {
        id: 26,
        question: "Vous pensez que généralement les autres",
        a1: "prennent beaucoup de temps pour apprendre à te connaître",
        a2: "un peu de temps pour te connaître",
      },
    ],
  },
  {
    id: 2,
    title:
      "Quel mot dans chaque paire vous plaît le plus? Pensez seulement à la signification des mots.",
    questions: [
      {
        id: 27,
        question: null,
        a1: "privé",
        a2: "ouvert",
      },
      {
        id: 28,
        question: null,
        a1: "planifié",
        a2: "non planifié",
      },
      {
        id: 29,
        question: null,
        a1: "abstrait",
        a2: "solide",
      },
      {
        id: 30,
        question: null,
        a1: "doux",
        a2: "ferme",
      },
      {
        id: 31,
        question: null,
        a1: "penser",
        a2: "sentir",
      },
      {
        id: 32,
        question: null,
        a1: "faits",
        a2: "idées",
      },
      {
        id: 33,
        question: null,
        a1: "impulsif",
        a2: "décision",
      },
      {
        id: 34,
        question: null,
        a1: "chaleureux",
        a2: "calme",
      },
      {
        id: 35,
        question: null,
        a1: "calme",
        a2: "sociable",
      },
      {
        id: 36,
        question: null,
        a1: "systématique",
        a2: "décontracté",
      },
      {
        id: 37,
        question: null,
        a1: "théorie",
        a2: "certitude",
      },
      {
        id: 38,
        question: null,
        a1: "sensible",
        a2: "juste",
      },
      {
        id: 39,
        question: null,
        a1: "convaincant",
        a2: "toucher",
      },
      {
        id: 40,
        question: null,
        a1: "déclaration",
        a2: "concept",
      },
      {
        id: 41,
        question: null,
        a1: "sans contrainte",
        a2: "prévu",
      },
      {
        id: 42,
        question: null,
        a1: "réservé",
        a2: "bavard",
      },
      {
        id: 43,
        question: null,
        a1: "ordonné",
        a2: "décontracté",
      },
      {
        id: 44,
        question: null,
        a1: "idées",
        a2: "actuallité",
      },
      {
        id: 45,
        question: null,
        a1: "compassion",
        a2: "prévoyance",
      },
      {
        id: 46,
        question: null,
        a1: "avantages",
        a2: "bénédictions",
      },
      {
        id: 47,
        question: null,
        a1: "no-nonsense",
        a2: "théorique",
      },
      {
        id: 48,
        question: null,
        a1: "quelques amis",
        a2: "beaucoup d'amis",
      },
      {
        id: 49,
        question: null,
        a1: "systématique",
        a2: "spontané",
      },
      {
        id: 50,
        question: null,
        a1: "imaginatif",
        a2: "en effet",
      },
      {
        id: 51,
        question: null,
        a1: "chaud",
        a2: "objectif",
      },
      {
        id: 52,
        question: null,
        a1: "objectif",
        a2: "passionné",
      },
      {
        id: 53,
        question: null,
        a1: "construire",
        a2: "inventer",
      },
      {
        id: 54,
        question: null,
        a1: "calme",
        a2: "sociable",
      },
      {
        id: 55,
        question: null,
        a1: "théorie",
        a2: "les faits",
      },
      {
        id: 56,
        question: null,
        a1: "compatissant",
        a2: "logique",
      },
      {
        id: 57,
        question: null,
        a1: "analytique",
        a2: "sentimental",
      },
      {
        id: 58,
        question: null,
        a1: "sensible",
        a2: "fascinant",
      },
    ],
  },
  {
    id: 3,
    title:
      "Quelle réponse correspond le mieux à la façon dont vous vous sentez ou agissez habituellement?",
    questions: [
      {
        id: 59,
        question:
          "Quand vous vous lancez sur un gros projet à remettre dans une semaine, vous allez",
        a1:
          "prendre le temps d'énumérer les choses à faire selon l'ordre des priorités ",
        a2: "commencer directement sans plan de travail",
      },
      {
        id: 60,
        question: "Avec les autres vous trouvez généralement",
        a1:
          "de la difficulté d'entamer et de maintenir une conversation avec certaines personnes",
        a2:
          "une facilité de parler à la plupart des gens pendant de longues périodes",
      },
      {
        id: 61,
        question:
          "En faisant quelque chose que beaucoup d'autres personnes le font, vous préférez davantage",
        a1: "le faire juste de manière acceptable",
        a2: "être créative",
      },
      {
        id: 62,
        question:
          "Les personnes que vous venez de rencontrer peuvent savoir ce qui peut vous intéresser",
        a1: "de suite",
        a2: "seulement après avoir appris à vous connaître",
      },
      {
        id: 63,
        question: "Vous préférez généralement des cours qui enseignent",
        a1: "les concepts et les principes",
        a2: "les faits avec des figures",
      },
      {
        id: 64,
        question: "Quel compliment vous correspond le plus",
        a1: "sentimentale",
        a2: "raisonnable",
      },
      {
        id: 65,
        question: "Vous trouvez que suivre un planning c'est",
        a1: "nécessaire à un moment mais généralement défavorable",
        a2: "favorable la plupart du temps",
      },
      {
        id: 66,
        question:
          "Quand vous êtes avec un groupe de personnes, préférez-vous habituellement",
        a1: "parler avec des gens que vous connaissez bien",
        a2: "participer à une discussion de groupe",
      },
      {
        id: 67,
        question: "Lors d'une soirée entre amis vous",
        a1: "parlez beaucoup",
        a2: "laissez les autres parler la plupart du temps",
      },
      {
        id: 68,
        question:
          "L'idée de faire une liste de ce que vous devriez faire pendant un week-end",
        a1: "est un choix",
        a2: "est ennuyeuse",
      },
      {
        id: 69,
        question: "Quel compliment vous correspond le plus",
        a1: "compétent",
        a2: "compatissant",
      },
      {
        id: 70,
        question: "Préférez-vous généralement",
        a1: "prendre de l'avance sur vos engagements ",
        a2: "être libre de faire ce que vous voulez sous l'impulsion du moment",
      },
      {
        id: 71,
        question:
          "Dans l'ensemble, lorsque vous travaillez sur une grosse mission, avez-vous tendance à",
        a1: "comprendre ce qui doit être fait au fur et à mesure",
        a2: "commencez par le décomposer en étapes",
      },
      {
        id: 72,
        question: "Pouvez-vous maintenir une conversation indéfiniment",
        a1: "uniquement avec des personnes qui partagent un certain intérêt",
        a2: "avec presque n'importe qui",
      },
      {
        id: 73,
        question: "Préférez-vous",
        a1: "soutenir les bonnes méthodes efficaces",
        a2: "analyser et attaquer les problèmes non résolus",
      },
      {
        id: 74,
        question: "Pour ton passe temps, tu aimes lire pour",
        a1: "profiter de façons étranges ou originales pour dire des choses",
        a2:
          "être comme les écrivains qui expriment ce qu'ils veulent à travers la lecture",
      },
      {
        id: 75,
        question:
          "Préférez-vous travailler avec un patron (ou un enseignant) qui est",
        a1: "gentil mais souvent dur",
        a2: "direct mais toujours rationnel",
      },
      {
        id: 76,
        question: "Préférez-vous faire la plupart des choses selon",
        a1: "votre humeur ce jour-là",
        a2: "un plan fixe",
      },
      {
        id: 77,
        question: "Vous pouvez",
        a1: "parler facilement et longtemps à presque n'importe qui ",
        a2:
          "parler facilement uniquement à certaines personnes ou sous une certaines conditions",
      },
      {
        id: 78,
        question: "Est-il plus important pour vous de prendre une décision",
        a1: "en pensant aux faits",
        a2: "en prenant en compte les sentiments et les opinions des autres",
      },
    ],
  },
  {
    id: 4,
    title:
      "Quel mot dans chaque paire vous plaît le plus? Pensez seulement à la signification des mots .",
    questions: [
      {
        id: 79,
        question: null,
        a1: "imaginatif",
        a2: "réaliste",
      },
      {
        id: 80,
        question: null,
        a1: "bruyant",
        a2: "ferme d'esprit",
      },
      {
        id: 81,
        question: null,
        a1: "raisonnable",
        a2: "bienveillant",
      },
      {
        id: 82,
        question: null,
        a1: "production",
        a2: "design",
      },
      {
        id: 83,
        question: null,
        a1: "possibilités",
        a2: "certitudes",
      },
      {
        id: 84,
        question: null,
        a1: "tendresse",
        a2: "force",
      },
      {
        id: 85,
        question: null,
        a1: "pratique",
        a2: "sentimental",
      },
      {
        id: 86,
        question: null,
        a1: "faire",
        a2: "créer",
      },
      {
        id: 87,
        question: null,
        a1: "roman",
        a2: "déjà connu",
      },
      {
        id: 88,
        question: null,
        a1: "sympathiser",
        a2: "analyser",
      },
      {
        id: 89,
        question: null,
        a1: "volontaire",
        a2: "tendre",
      },
      {
        id: 90,
        question: null,
        a1: "concrètes",
        a2: "abstrait",
      },
      {
        id: 91,
        question: null,
        a1: "dévoué",
        a2: "déterminé",
      },
      {
        id: 92,
        question: null,
        a1: "compétent",
        a2: "bon cœur",
      },
      {
        id: 93,
        question: null,
        a1: "pratique",
        a2: "innovant",
      },
    ],
  },
];

// const MBTI_FORM: MBTIForm = [
//   {
//     id: 1,
//     title:
//       "Which answer comes closest to describing how you usually feel or act ?",
//     questions: [
//       {
//         id: 1,
//         question: "When you go somewhere for the day, would you rather",
//         a1: "plan what you will do and when",
//         a2: "just go",
//       },
//       {
//         id: 2,
//         question: "Do you consider yourself to be",
//         a1: "more of a spontaneous person",
//         a2: "more of an organised person",
//       },
//       {
//         id: 3,
//         question: "if you were a teacher, would you rather teach",
//         a1: "fact courses",
//         a2: "courses involving theory",
//       },
//       {
//         id: 4,
//         question: "are you usually",
//         a1: `a "good mixer"`,
//         a2: "rather quiet and reserved",
//       },
//       {
//         id: 5,
//         question: "do you usually get along better with",
//         a1: "imaginative people",
//         a2: "realistic people",
//       },
//       {
//         id: 6,
//         question: "do you more often let",
//         a1: "your heart rule your head",
//         a2: "your head rule your heart",
//       },
//       {
//         id: 7,
//         question: "do you prefer to do many things",
//         a1: "one the spur of the moment",
//         a2: "according to your plans",
//       },
//       {
//         id: 8,
//         question: "are you",
//         a1: "easy to get to know",
//         a2: "hard to get to know",
//       },
//       {
//         id: 9,
//         question: "Does following a schedule",
//         a1: "appeal to you",
//         a2: "cramp you",
//       },
//       {
//         id: 10,
//         question: "when you have a special job to do, do you like to",
//         a1: "organise it carefully before you start",
//         a2: "find out what is necessary as you go along",
//       },
//       {
//         id: 11,
//         question: "in most instances, do you prefer to",
//         a1: "go with the flow",
//         a2: "follow a schedule",
//       },
//       {
//         id: 12,
//         question: "would most people say you are",
//         a1: "a private person",
//         a2: "a very open person",
//       },
//       {
//         id: 13,
//         question: "would you rather be considered",
//         a1: "a practical person",
//         a2: "an ingenious person",
//       },
//       {
//         id: 14,
//         question: "in a large group do you more often",
//         a1: "introduce others",
//         a2: "get introduced",
//       },
//       {
//         id: 15,
//         question: "would you rather have as friend someone who",
//         a1: "is always coming up with new ideas",
//         a2: "has both feet on the ground",
//       },
//       {
//         id: 16,
//         question: "are you inclined to",
//         a1: "value sentiment more than logic",
//         a2: "value logic more than sentiment",
//       },
//       {
//         id: 17,
//         question: "do you prefet to",
//         a1: "wait and see what happens and then make plans",
//         a2: "plan things far in advance",
//       },
//       {
//         id: 18,
//         question: "do you tend to spend a lot of time",
//         a1: "by yourself",
//         a2: "with others",
//       },
//       {
//         id: 19,
//         question: "do you find being around a lot of people",
//         a1: "give you more energy",
//         a2: `is often "draining"`,
//       },
//       {
//         id: 20,
//         question: "do you prefer to",
//         a1: "arrange dates, partie, etc.",
//         a2: "be free to do whatever looks like fun when the time comes",
//       },
//       {
//         id: 21,
//         question: "in planing a trip would you prefer to",
//         a1: "most of the time do whatever you feel like that day",
//         a2: "know ahead of time what you'll be doing most days",
//       },
//       {
//         id: 22,
//         question: "at partie, do you",
//         a1: "sometimes get bored",
//         a2: "always have fun",
//       },
//       {
//         id: 23,
//         question: "do you usually",
//         a1: "mingle well with others",
//         a2: "tend to keep more to yourself",
//       },
//       {
//         id: 24,
//         question: "are you more attracted to",
//         a1: "a person with a quick and brilliant mind",
//         a2: "a practical person with a lot of common sense",
//       },
//       {
//         id: 25,
//         question: "in you daily work, do you",
//         a1: "rather enjoy an emergency that makes you work against time",
//         a2: "usually plan your work so you won't need to work under pressure",
//       },
//       {
//         id: 26,
//         question: "would you say it generally takes others",
//         a1: "a lot of time to get to know you",
//         a2: "a little time to get to know you",
//       },
//     ],
//   },
//   {
//     id: 2,
//     title:
//       "Which word in each pair appeals to you more? Think about what the words mean, not about how they look or how they sound.",
//     questions: [
//       {
//         id: 27,
//         question: null,
//         a1: "private",
//         a2: "open",
//       },
//       {
//         id: 28,
//         question: null,
//         a1: "schedules",
//         a2: "unplanned",
//       },
//       {
//         id: 29,
//         question: null,
//         a1: "abstract",
//         a2: "solid",
//       },
//       {
//         id: 30,
//         question: null,
//         a1: "gentle",
//         a2: "firm",
//       },
//       {
//         id: 31,
//         question: null,
//         a1: "thinking",
//         a2: "feeling",
//       },
//       {
//         id: 32,
//         question: null,
//         a1: "facts",
//         a2: "ideas",
//       },
//       {
//         id: 33,
//         question: null,
//         a1: "impulsive",
//         a2: "decision",
//       },
//       {
//         id: 34,
//         question: null,
//         a1: "hearty",
//         a2: "quiet",
//       },
//       {
//         id: 35,
//         question: null,
//         a1: "quiet",
//         a2: "outgoing",
//       },
//       {
//         id: 36,
//         question: null,
//         a1: "systematic",
//         a2: "casual",
//       },
//       {
//         id: 37,
//         question: null,
//         a1: "theory",
//         a2: "certainty",
//       },
//       {
//         id: 38,
//         question: null,
//         a1: "sensitive",
//         a2: "just",
//       },
//       {
//         id: 39,
//         question: null,
//         a1: "convincing",
//         a2: "touching",
//       },
//       {
//         id: 40,
//         question: null,
//         a1: "statement",
//         a2: "concept",
//       },
//       {
//         id: 41,
//         question: null,
//         a1: "unconstrained",
//         a2: "scheduled",
//       },
//       {
//         id: 42,
//         question: null,
//         a1: "reserved",
//         a2: "talkative",
//       },
//       {
//         id: 43,
//         question: null,
//         a1: "orderly",
//         a2: "easygoing",
//       },
//       {
//         id: 44,
//         question: null,
//         a1: "ideas",
//         a2: "actuallity",
//       },
//       {
//         id: 45,
//         question: null,
//         a1: "compassion",
//         a2: "foresight",
//       },
//       {
//         id: 46,
//         question: null,
//         a1: "benefits",
//         a2: "blessings",
//       },
//       {
//         id: 47,
//         question: null,
//         a1: "no-nonsense",
//         a2: "theoritical",
//       },
//       {
//         id: 48,
//         question: null,
//         a1: "few friends",
//         a2: "lots of friends",
//       },
//       {
//         id: 49,
//         question: null,
//         a1: "systematic",
//         a2: "spontaneous",
//       },
//       {
//         id: 50,
//         question: null,
//         a1: "imaginative",
//         a2: "matter-of-fact",
//       },
//       {
//         id: 51,
//         question: null,
//         a1: "warm",
//         a2: "objective",
//       },
//       {
//         id: 52,
//         question: null,
//         a1: "objective",
//         a2: "passionate",
//       },
//       {
//         id: 53,
//         question: null,
//         a1: "build",
//         a2: "invent",
//       },
//       {
//         id: 54,
//         question: null,
//         a1: "quiet",
//         a2: "gregarious",
//       },
//       {
//         id: 55,
//         question: null,
//         a1: "theory",
//         a2: "fact",
//       },
//       {
//         id: 56,
//         question: null,
//         a1: "compassionate",
//         a2: "logical",
//       },
//       {
//         id: 57,
//         question: null,
//         a1: "analytical",
//         a2: "sentimental",
//       },
//       {
//         id: 58,
//         question: null,
//         a1: "sensible",
//         a2: "fascinating",
//       },
//     ],
//   },
//   {
//     id: 3,
//     title:
//       "Which answer comes closest to describing how you usually feel or act ?",
//     questions: [
//       {
//         id: 59,
//         question: "when you start a big project that is due in a week, do you",
//         a1:
//           "take time to list separate things to be done and the order of doing them",
//         a2: "plunge right in",
//       },
//       {
//         id: 60,
//         question: "in social sitations do you generally find it",
//         a1: "difficult to start and maintain a conversation with some people",
//         a2: "easy to talk to most people for long periods of time",
//       },
//       {
//         id: 61,
//         question:
//           "in doing something that many other people do, does it appeal to you more to",
//         a1: "do it in the accepted way",
//         a2: "invent a way of your own",
//       },
//       {
//         id: 62,
//         question: "can the new people you meet tell what you are interest in",
//         a1: "right away",
//         a2: "only after they get to know you",
//       },
//       {
//         id: 63,
//         question: "do you generally prefer courses that teach",
//         a1: "concepts and principles",
//         a2: "facts and figures",
//       },
//       {
//         id: 64,
//         question: "is it a higher compliment to be called",
//         a1: "a person of real feeling",
//         a2: "a consistently reasonable person",
//       },
//       {
//         id: 65,
//         question: "do you find going by a schedule",
//         a1: "necessary at time but generally unfavourable",
//         a2: "helful and favourable most of the time",
//       },
//       {
//         id: 66,
//         question:
//           "when you are with a group of people, would you usually rather",
//         a1: "talk individually with people you know well",
//         a2: "join in the talk of the group",
//       },
//       {
//         id: 67,
//         question: "at parties do you",
//         a1: "do much of the talking",
//         a2: "let others do most of the talking",
//       },
//       {
//         id: 68,
//         question:
//           "does the idea of making a list of what you should get done over a weekend",
//         a1: "appeal to you",
//         a2: "leave you cold",
//       },
//       {
//         id: 69,
//         question: "which is a higher compliment, to be called",
//         a1: "competent",
//         a2: "compassionate",
//       },
//       {
//         id: 70,
//         question: "do you generally prefer to",
//         a1: "make your social engagements some distance ahead",
//         a2: "be free to do thigns on the spur of the moment",
//       },
//       {
//         id: 71,
//         question: "overall, when working on a big assignement, do you tend to",
//         a1: "figure out what needs to be done as you go along",
//         a2: "begin by breaking it down into steps",
//       },
//       {
//         id: 72,
//         question: "can you keep a conversation going indefinitely",
//         a1: "only with people who share some interest of yours",
//         a2: "with almost anyone",
//       },
//       {
//         id: 73,
//         question: "would you rather",
//         a1: "support the established methods of doing good",
//         a2: "analyze what is still wrong and attack unsolved problems",
//       },
//       {
//         id: 74,
//         question: "in reading for pleasure, do you",
//         a1: "enjoy odd or original ways of saying things",
//         a2: "like writers to say exactly what they mean",
//       },
//       {
//         id: 75,
//         question: "would you rather work under a boss (or teacher) who is",
//         a1: "good-natured but often inconsistant",
//         a2: "sharp-tongued but always logical",
//       },
//       {
//         id: 76,
//         question: "would you prefers to do most things according to",
//         a1: "however you feel that particular day",
//         a2: "a set schedule",
//       },
//       {
//         id: 77,
//         question: "can you",
//         a1: "talk easily to almost anyone for as long as you have to",
//         a2:
//           "find a lot to say only to certain people or under certain conditions",
//       },
//       {
//         id: 78,
//         question: "when making a decision, is it more important to you to",
//         a1: "weigh the facts",
//         a2: "consider people feelings and opinions",
//       },
//     ],
//   },
//   {
//     id: 4,
//     title:
//       "Which word in each pair appeals to you more ? Think about what the words mean, not about how they look or how they sound.",
//     questions: [
//       {
//         id: 79,
//         question: null,
//         a1: "imaginative",
//         a2: "realistic",
//       },
//       {
//         id: 80,
//         question: null,
//         a1: "bighearted",
//         a2: "firm-minded",
//       },
//       {
//         id: 81,
//         question: null,
//         a1: "fair-minded",
//         a2: "caring",
//       },
//       {
//         id: 82,
//         question: null,
//         a1: "production",
//         a2: "design",
//       },
//       {
//         id: 83,
//         question: null,
//         a1: "possibilities",
//         a2: "certainties",
//       },
//       {
//         id: 84,
//         question: null,
//         a1: "tenderness",
//         a2: "strengh",
//       },
//       {
//         id: 85,
//         question: null,
//         a1: "pratical",
//         a2: "sentimental",
//       },
//       {
//         id: 86,
//         question: null,
//         a1: "make",
//         a2: "create",
//       },
//       {
//         id: 87,
//         question: null,
//         a1: "novel",
//         a2: "already known",
//       },
//       {
//         id: 88,
//         question: null,
//         a1: "sympathise",
//         a2: "analyse",
//       },
//       {
//         id: 89,
//         question: null,
//         a1: "strong-willed",
//         a2: "tenderhearted",
//       },
//       {
//         id: 90,
//         question: null,
//         a1: "concrete",
//         a2: "abstract",
//       },
//       {
//         id: 91,
//         question: null,
//         a1: "devoted",
//         a2: "determined",
//       },
//       {
//         id: 92,
//         question: null,
//         a1: "competent",
//         a2: "kindhearted",
//       },
//       {
//         id: 93,
//         question: null,
//         a1: "practical",
//         a2: "innovative",
//       },
//     ],
//   },
// ];

const MBTI_VALIDATION: MBTIValidation = [
  {
    id: 1,
    validators: [
      {
        id: 1,
        v1: "J",
        v2: "P",
      },
      {
        id: 2,
        v1: "P",
        v2: "J",
      },
      {
        id: 3,
        v1: "S",
        v2: "N",
      },
      {
        id: 4,
        v1: "E",
        v2: "I",
      },
      {
        id: 5,
        v1: "N",
        v2: "S",
      },
      {
        id: 6,
        v1: "F",
        v2: "T",
      },
      {
        id: 7,
        v1: "P",
        v2: "J",
      },
      {
        id: 8,
        v1: "E",
        v2: "I",
      },
      {
        id: 9,
        v1: "J",
        v2: "P",
      },
      {
        id: 10,
        v1: "J",
        v2: "P",
      },
      {
        id: 11,
        v1: "P",
        v2: "J",
      },
      {
        id: 12,
        v1: "I",
        v2: "E",
      },
      {
        id: 13,
        v1: "S",
        v2: "N",
      },
      {
        id: 14,
        v1: "E",
        v2: "I",
      },
      {
        id: 15,
        v1: "N",
        v2: "S",
      },
      {
        id: 16,
        v1: "F",
        v2: "T",
      },
      {
        id: 17,
        v1: "P",
        v2: "J",
      },
      {
        id: 18,
        v1: "I",
        v2: "E",
      },
      {
        id: 19,
        v1: "E",
        v2: "I",
      },
      {
        id: 20,
        v1: "J",
        v2: "P",
      },
      {
        id: 21,
        v1: "P",
        v2: "J",
      },
      {
        id: 22,
        v1: "I",
        v2: "E",
      },
      {
        id: 23,
        v1: "E",
        v2: "I",
      },
      {
        id: 24,
        v1: "N",
        v2: "S",
      },
      {
        id: 25,
        v1: "P",
        v2: "J",
      },
      {
        id: 26,
        v1: "I",
        v2: "E",
      },
    ],
  },
  {
    id: 2,
    validators: [
      {
        id: 27,
        v1: "I",
        v2: "E",
      },
      {
        id: 28,
        v1: "J",
        v2: "P",
      },
      {
        id: 29,
        v1: "N",
        v2: "S",
      },
      {
        id: 30,
        v1: "F",
        v2: "T",
      },
      {
        id: 31,
        v1: "T",
        v2: "F",
      },
      {
        id: 32,
        v1: "S",
        v2: "N",
      },
      {
        id: 33,
        v1: "P",
        v2: "J",
      },
      {
        id: 34,
        v1: "E",
        v2: "I",
      },
      {
        id: 35,
        v1: "I",
        v2: "E",
      },
      {
        id: 36,
        v1: "J",
        v2: "P",
      },
      {
        id: 37,
        v1: "N",
        v2: "S",
      },
      {
        id: 38,
        v1: "F",
        v2: "T",
      },
      {
        id: 39,
        v1: "T",
        v2: "F",
      },
      {
        id: 40,
        v1: "S",
        v2: "N",
      },
      {
        id: 41,
        v1: "P",
        v2: "J",
      },
      {
        id: 42,
        v1: "I",
        v2: "E",
      },
      {
        id: 43,
        v1: "J",
        v2: "P",
      },
      {
        id: 44,
        v1: "N",
        v2: "S",
      },
      {
        id: 45,
        v1: "F",
        v2: "T",
      },
      {
        id: 46,
        v1: "T",
        v2: "F",
      },
      {
        id: 47,
        v1: "S",
        v2: "N",
      },
      {
        id: 48,
        v1: "I",
        v2: "E",
      },
      {
        id: 49,
        v1: "J",
        v2: "P",
      },
      {
        id: 50,
        v1: "N",
        v2: "S",
      },
      {
        id: 51,
        v1: "F",
        v2: "T",
      },
      {
        id: 52,
        v1: "T",
        v2: "F",
      },
      {
        id: 53,
        v1: "S",
        v2: "N",
      },
      {
        id: 54,
        v1: "I",
        v2: "E",
      },
      {
        id: 55,
        v1: "N",
        v2: "S",
      },
      {
        id: 56,
        v1: "F",
        v2: "T",
      },
      {
        id: 57,
        v1: "T",
        v2: "F",
      },
      {
        id: 58,
        v1: "S",
        v2: "N",
      },
    ],
  },
  {
    id: 3,
    validators: [
      {
        id: 59,
        v1: "J",
        v2: "P",
      },
      {
        id: 60,
        v1: "I",
        v2: "E",
      },
      {
        id: 61,
        v1: "S",
        v2: "N",
      },
      {
        id: 62,
        v1: "E",
        v2: "I",
      },
      {
        id: 63,
        v1: "N",
        v2: "S",
      },
      {
        id: 64,
        v1: "F",
        v2: "T",
      },
      {
        id: 65,
        v1: "P",
        v2: "J",
      },
      {
        id: 66,
        v1: "I",
        v2: "E",
      },
      {
        id: 67,
        v1: "E",
        v2: "I",
      },
      {
        id: 68,
        v1: "J",
        v2: "P",
      },
      {
        id: 69,
        v1: "T",
        v2: "F",
      },
      {
        id: 70,
        v1: "J",
        v2: "P",
      },
      {
        id: 71,
        v1: "P",
        v2: "J",
      },
      {
        id: 72,
        v1: "I",
        v2: "E",
      },
      {
        id: 73,
        v1: "S",
        v2: "N",
      },
      {
        id: 74,
        v1: "N",
        v2: "S",
      },
      {
        id: 75,
        v1: "F",
        v2: "T",
      },
      {
        id: 76,
        v1: "P",
        v2: "J",
      },
      {
        id: 77,
        v1: "E",
        v2: "I",
      },
      {
        id: 78,
        v1: "T",
        v2: "F",
      },
    ],
  },
  {
    id: 4,
    validators: [
      {
        id: 79,
        v1: "N",
        v2: "S",
      },
      {
        id: 80,
        v1: "F",
        v2: "T",
      },
      {
        id: 81,
        v1: "T",
        v2: "F",
      },
      {
        id: 82,
        v1: "S",
        v2: "N",
      },
      {
        id: 83,
        v1: "N",
        v2: "S",
      },
      {
        id: 84,
        v1: "F",
        v2: "T",
      },
      {
        id: 85,
        v1: "T",
        v2: "F",
      },
      {
        id: 86,
        v1: "S",
        v2: "N",
      },
      {
        id: 87,
        v1: "N",
        v2: "S",
      },
      {
        id: 88,
        v1: "F",
        v2: "T",
      },
      {
        id: 89,
        v1: "T",
        v2: "F",
      },
      {
        id: 90,
        v1: "S",
        v2: "N",
      },
      {
        id: 91,
        v1: "F",
        v2: "T",
      },
      {
        id: 92,
        v1: "T",
        v2: "F",
      },
      {
        id: 93,
        v1: "S",
        v2: "N",
      },
    ],
  },
];

const MBTI_VALIDATOR = (pi: number, vi: number, a: "a1" | "a2") => {
  return MBTI_VALIDATION[pi].validators[vi][a === "a1" ? "v1" : "v2"];
};

export {
  MBTI_FORM,
  MBTI_VALIDATOR,
  MBTIPoints,
  MBTI_PERSONALITY,
  MBTIPersonality,
};
