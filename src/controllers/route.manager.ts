import {
  RouteVerifier,
  RouteMethod,
  IServerResponse,
  RouteKey,
  ROUTES as R,
  IUserInfo,
} from "polyteam-shared";
import { RequestHandler, Express, Response, Request } from "express";
import { StrictMap } from "../utils/type.utils";

type ReceiveHandler<I, O> = (
  input: I,
  params?: ICustomReqParams
) => Promise<IServerResponse<O>>;

export interface ICustomRequest extends Request {
  identity?: IUserInfo;
}

export interface ICustomReqParams {
  req: ICustomRequest;
  res: Response;
  _: () => any;
}

export class BackendReceiver<I, O> implements RouteVerifier<I, O> {
  url: string;
  method: RouteMethod;
  in: (o: any) => o is I;
  out: (o: any) => o is IServerResponse<O>;

  public constructor(v: RouteVerifier<I, O>) {
    this.url = v.url;
    this.method = v.method;
    this.in = v.in;
    this.out = v.out;
  }

  public register(a: Express, h: ReceiveHandler<I, O>, g?: RequestHandler) {
    const route = a.route(this.url);
    const params = [...(g ? [g] : []), this.transform(h)];
    return route[this.method](...params);
  }

  private transform(handler: ReceiveHandler<I, O>): RequestHandler {
    return async (req: ICustomRequest, res: Response, _: () => any) => {
      console.log(req.body);

      if (!this.in(req.body)) return res.status(400).send();

      const params: ICustomReqParams = { req, res, _ };

      let code: number, msg: string, data: O;

      try {
        ({ code, msg, data } = await handler(req.body, params));
      } catch (e) {
        console.log(e);
        code = 500;
      }

      if (code < 300 && !this.out({ code, msg, data })) {
        console.log(code, msg, data);
        res.status(500).json({});
      } else res.status(code).json({ msg, data });
    };
  }
}

export class Routes implements StrictMap<RouteKey, BackendReceiver<any, any>> {
  login = new BackendReceiver(R.login);
  token = new BackendReceiver(R.token);
  suggestion = new BackendReceiver(R.suggestion);
  mbtiForm = new BackendReceiver(R.mbtiForm);
  mbtiProcess = new BackendReceiver(R.mbtiProcess);
  admin = new BackendReceiver(R.admin);
  teacher = new BackendReceiver(R.teacher);
  teacherList = new BackendReceiver(R.teacherList);
  upload = new BackendReceiver(R.upload);
  updateUser = new BackendReceiver(R.updateUser);
  likeUser = new BackendReceiver(R.likeUser);
  getTeam = new BackendReceiver(R.getTeam);
  getTeams = new BackendReceiver(R.getTeams);
  updateTeams = new BackendReceiver(R.updateTeams);
  createCode = new BackendReceiver(R.createCode);
  joinCode = new BackendReceiver(R.joinCode);
  validateTeam = new BackendReceiver(R.validateTeam);
  createTeams = new BackendReceiver(R.createTeams);
  listAllUsers = new BackendReceiver(R.listAllUsers);
  reset = new BackendReceiver(R.reset);
}

export const ROUTES = new Routes();
