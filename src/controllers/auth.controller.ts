import { Response } from "express";
import { AuthService } from "../services/auth.service";
import {
  LoginType,
  isString,
  ILoginForm,
  ITokenForm,
  IUserInfo,
} from "polyteam-shared";
import { ICustomReqParams, ICustomRequest as Request } from "./route.manager";

function guard(type?: LoginType, login = false) {
  const f = async (req: Request, res: Response, n: Function) => {
    const token = req.headers.authorization;

    if (!isString(token)) {
      res.status(401).send();
      req.connection.destroy();
      return;
    }

    let code: number, msg: string, data: IUserInfo;
    const types: LoginType[] = type ? [type] : ["ADMIN", "TEACHER", "USER"];

    while (types.length > 0) {
      type = types.pop();
      ({ code, msg, data } = await AuthService.tokenAuth({ token, type }));

      if (code === 200) break;
    }

    if (code !== 200 || login) {
      res.status(code).json({ msg, data });
      req.connection.destroy();
    } else {
      req.identity = data;
      n();
    }
  };

  return f;
}

export default class AuthController {
  public async login(input: ILoginForm) {
    return await AuthService.loginAuth(input);
  }

  public async token(f: ITokenForm, p?: ICustomReqParams) {
    const { req } = p;
    const token = req.headers.authorization;

    if (!isString(token)) return { code: 401 };

    const type = f.loginType;
    return await AuthService.tokenAuth({ token, type });
  }
}

export const authController = new AuthController();
export const adminGuard = guard("ADMIN");
export const teacherGuard = guard("TEACHER");
export const userGuard = guard("USER");
export const anyGuard = guard();
