import {
  ISuggestionForm,
  isString,
  IUserLike,
  IServerResponse,
  IUserPublicInfo,
  ITeamCreationResponse,
  IGetTeam,
  IJoinCode,
  ICourseTeam,
  IUpdateTeams,
} from "polyteam-shared";
import { SuggestionService } from "../services/suggestion.service";
import { ICustomReqParams } from "./route.manager";
import { TeamManagerService } from "../services/teamManager.service";
import { CodeManagerService } from "../services/codeManager.service";

export default class GroupController {
  public async suggestion(
    input: ISuggestionForm,
    params: ICustomReqParams
  ): Promise<IServerResponse<IUserPublicInfo[][]>> {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500, data: [] };
    }

    const data = await SuggestionService.run(req.identity.email, input);
    const code = data ? 200 : 404;
    return { code, data };
  }

  public async likeUser(
    input: IUserLike,
    params: ICustomReqParams
  ): Promise<IServerResponse<ITeamCreationResponse>> {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500 };
    }

    const data = await TeamManagerService.likeUser(req.identity.email, input);
    const code = data ? 200 : 400;

    return { code, data };
  }

  public async validateTeam(input: IGetTeam, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500 };
    }

    const data = await TeamManagerService.validateTeam(
      req.identity.email,
      input.courseId
    );
    const code = data ? 200 : 400;

    return { code };
  }

  public async getTeam(input: IGetTeam, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500 };
    }

    const data = await TeamManagerService.getTeam(
      req.identity.email,
      input.courseId
    );
    const code = data ? 200 : 404;

    return { code, data };
  }

  public async getTeams(input: IGetTeam, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500 };
    }

    const data = await TeamManagerService.getTeams(
      req.identity.email,
      input.courseId
    );

    const code = data ? 200 : 404;

    return { code, data };
  }

  public async updateTeams(input: IUpdateTeams, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500 };
    }

    const data = await TeamManagerService.updateTeams(
      req.identity.email,
      input.courseId,
      input.teams
    );

    const code = data ? 200 : 404;

    return { code };
  }

  public async createTeams(input: IGetTeam, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500 };
    }

    const data = await TeamManagerService.createTeams(
      req.identity.email,
      input.courseId
    );

    const code = data ? 200 : 404;

    return { code };
  }

  public async createCode(input: IGetTeam, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500 };
    }

    const [msg, code] = await CodeManagerService.createCode(
      req.identity.email,
      input.courseId
    );

    return { code, msg };
  }

  public async joinCode(input: IJoinCode, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return { code: 500 };
    }

    const [msg, code] = await CodeManagerService.joinCode(
      req.identity.email,
      input.code
    );

    return { code, msg };
  }
}

export const groupController = new GroupController();
