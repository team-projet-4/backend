import { UserManagerService } from "../services/userManager.service";
import {
  isString,
  ICreateAccount,
  ICreateUserGroup,
  makeResponse,
  IUpdateAccount,
} from "polyteam-shared";
import { CourseManagerService } from "../services/courseManager.service";
import { ICustomReqParams } from "./route.manager";
import { BackupService } from "../services/backup.service";
import { IBackup } from "models/backup.model";

export default class UserController {
  public async reset(backup: IBackup) {
    console.log(backup);

    const res = await BackupService.reset(backup.key);
    const code = res ? 200 : 409;

    return { code };
  }

  public async createAdmin(input: ICreateAccount) {
    const [res, _] = await UserManagerService.create(input, "ADMIN");
    const code = res ? 200 : 409;
    return { code };
  }

  public async listAllUsers(_: any) {
    const data = await UserManagerService.listAllUsers();
    const code = 200;

    return { code, data };
  }

  public async createTeacher(input: ICreateAccount) {
    const [res, _] = await UserManagerService.create(input, "TEACHER");
    const code = res ? 200 : 409;
    return { code };
  }

  public async uploadList(input: ICreateUserGroup, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return makeResponse<any>(500);
    }

    const [code, msg] = await CourseManagerService.createOrUpdate(
      input,
      req.identity.email
    );

    return { code, msg };
  }

  public async updateUser(input: IUpdateAccount, params: ICustomReqParams) {
    const { req } = params;

    if (!req.identity || !isString(req.identity.email)) {
      return makeResponse<string>(500);
    }

    const data = await UserManagerService.update(req.identity.email, input);
    const code = data ? 200 : 404;

    return { code, data };
  }
}

export const userController = new UserController();
