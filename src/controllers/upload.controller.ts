import { ICustomReqParams } from "./route.manager";
import { FileService } from "../services/file.service";
import { isArray } from "util";

export default class UploadController {
  public async uploadFile(_: any, params: ICustomReqParams) {
    const { req } = params;

    if (!req.files || !req.files.upload || isArray(req.files.upload))
      return { code: 400 };

    const data = FileService.save(req.files.upload);
    const code = data ? 200 : 500;

    return { code, data };
  }
}

export const uploadController = new UploadController();
