import { MBTIService } from "../services/mbti.service";
import {
  isString,
  MBTIAnswerForm,
  makeResponse,
  PersonalityType,
} from "polyteam-shared";
import { ICustomReqParams } from "./route.manager";

export default class MBTIController {
  public async getForm(_: any) {
    const data = MBTIService.getMBTIForm();
    const code = data ? 200 : 500;
    return { code, data };
  }

  public async process(input: MBTIAnswerForm, params: ICustomReqParams) {
    console.log("TEST");
    const { req } = params;

    if (!req.identity || !isString(req.identity.email))
      return makeResponse<PersonalityType>(500);

    const data = await MBTIService.submitForm(req.identity.email, input);
    const code = data ? 200 : 404;

    return { code, data };
  }
}

export const mbtiController = new MBTIController();
