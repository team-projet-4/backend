import { IUserLike, ICourseTeam, IUserGroupSettings } from "polyteam-shared";
import { User, ILikeUser, IUser } from "../models/user.model";
import { CourseManagerService } from "./courseManager.service";
import { Course } from "../models/course.model";
import { TeamGenerationService } from "./teamGeneration.service";

export class TeamManagerService {
  public static async getTeam(email: string, courseId: string) {
    const user = await User.findOne({ email }).exec();
    const course = await Course.findOne({ courseId }).exec();

    console.log(user, course);
    if (!user || !course) return undefined;

    return course.teams.find(
      t => t.users.findIndex(u => u.email === email) !== -1
    );
  }

  public static async getTeams(email: string, courseId: string) {
    const user = await User.findOne({ email }).exec();
    const course = await Course.findOne({ courseId }).exec();

    if (!user || !course || course.teacher !== email) return undefined;

    return course.teams;
  }

  public static async updateTeams(
    email: string,
    courseId: string,
    teams: ICourseTeam[]
  ) {
    const user = await User.findOne({ email }).exec();
    const course = await Course.findOne({ courseId }).exec();

    if (!user || !course || course.teacher !== email) return undefined;

    // TODO CHECK IF WE SHOULD SET THE TEAMS TO READY AFTER UPDATE
    teams.forEach(t => {
      t.ready = true;
      t.users.forEach(u => (u.validated = true));
    });

    course.teams = teams.filter(t => t.users.length > 0);
    await course.save();

    return true;
  }

  public static async createTeams(email: string, courseId: string) {
    const user = await User.findOne({ email }).exec();
    const course = await Course.findOne({ courseId }).exec();

    if (!user || !course || course.teacher !== email) return undefined;

    const users = await User.find()
      .where("email")
      .in(course.users)
      .exec();

    const chunks =
      course.settings.algorithm === "MBTI"
        ? TeamGenerationService.mbti(users, course.settings.nbUsers)
        : TeamGenerationService.random(users, course.settings.nbUsers);

    const ready = true;
    const validated = true;
    const teams: ICourseTeam[] = chunks.map(c => ({
      ready,
      users: c.map(u => ({
        email: u.email,
        fname: u.fname,
        lname: u.lname,
        validated,
      })),
    }));

    course.teams = teams;

    await Course.updateOne({ courseId }, course).exec();

    return true;
  }

  public static async validateTeam(email: string, courseId: string) {
    const user = await User.findOne({ email }).exec();
    const course = await Course.findOne({ courseId }).exec();

    if (!user || !course) return undefined;

    const team = course.teams.find(t => t.users.find(u => u.email === email));
    if (!team) return undefined;

    const teamMember = team.users.find(u => u.email === email);
    if (!teamMember) return undefined;

    teamMember.validated = true;

    if (
      team.users.every(u => u.validated) &&
      this.assertTeam(team, course.settings)
    )
      team.ready = true;

    await Course.updateOne({ courseId }, course).exec();

    return true;
  }

  public static async likeUser(email: string, like: IUserLike) {
    // Add Like to user
    const user = await User.findOne({ email }).exec();
    if (!user) return undefined;
    if (!user.likedUsers[like.courseId]) user.likedUsers[like.courseId] = [];

    console.log(user.likedUsers[like.courseId]);

    const emails = [like.userEmail];
    const factor = like.likeFactor;

    const likes = emails.map(e => ({ email: e, factor }));
    likes.forEach(l => user.likedUsers[like.courseId].push(l));

    console.log(user.likedUsers[like.courseId]);

    await User.updateOne({ email }, user).exec();

    // Check for matches
    const course = await CourseManagerService.get(like.courseId);

    const teamIndex1 = course.teams.findIndex(
      t => t.users.findIndex(u => u.email === email) !== -1
    );

    const teamIndex2 = course.teams.findIndex(
      t => t.users.findIndex(u => u.email === emails[0]) !== -1
    );

    // Check if same team
    if (teamIndex1 === teamIndex2) return undefined;

    const team1 = course.teams[teamIndex1];
    const team2 = course.teams[teamIndex2];

    console.log("______TEAMS_______");
    console.log(team1);
    console.log(team2);

    // CHECKING IF TEAMS ARE CORRECT
    if (!team1 || !team2) return undefined;
    if (team1.ready) return undefined;

    if (team2.ready) return { matched: false };
    if (team1.users.length + team2.users.length > course.settings.nbUsers.upper)
      return { matched: false };

    // CHECKING THE TEAMS LIKE RATIO
    const likedUsers1 = await this.fetchLikedUsers(team1, like.courseId);
    const likedUsers2 = await this.fetchLikedUsers(team2, like.courseId);

    console.log("______LIKEDUSERS_______");
    console.log(likedUsers1);
    console.log(likedUsers2);

    if (
      !this.calculateLike(likedUsers1, team2) ||
      !this.calculateLike(likedUsers2, team1)
    )
      return { matched: false };

    // MERGING TEAMS
    [team1, team2].forEach(t => t.users.forEach(u => (u.validated = false)));
    const users = [...team1.users, ...team2.users];
    const ready = false;

    course.teams[teamIndex1] = { users, ready };
    course.teams.splice(teamIndex2, 1);

    try {
      await course.save();
    } catch (e) {
      console.log(e);
    }

    return { matched: true };
  }

  private static calculateLike(likedUsers: ILikeUser[][], team: ICourseTeam) {
    let sum = 0;
    const emails = team.users.map(u => u.email);

    likedUsers.forEach(lus => {
      emails.forEach(email => {
        const l = lus.find(lu => lu.email === email);
        sum += l ? l.factor : -2;
      });
    });

    console.log(sum);
    return sum >= 0;
  }

  private static async fetchLikedUsers(team: ICourseTeam, course: string) {
    const users = await Promise.all(
      team.users.map(u => User.findOne({ email: u.email }).exec())
    );

    return users
      .map(u => u.likedUsers)
      .map(l => l[course] || ([] as ILikeUser[]));
  }

  private static assertTeam(team: ICourseTeam, settings: IUserGroupSettings) {
    const len = team.users.length;
    const nb = len >= settings.nbUsers.lower && len <= settings.nbUsers.upper;

    return nb;
  }
}
