import { chunk, shuffle, flatten, mean } from "lodash";
import { IUser } from "../models/user.model";
import { RandomUtils } from "../utils/random.utils";
import { ObjectUtils } from "../utils/object.utils";
import { MBTI_PERSONALITY } from "../config/mbti";
import { MBTI_HELPER } from "./mbti.service";
import { Quadruplet, Range } from "../utils/type.utils";

export class TeamGenerationService {
  public static random(users: IUser[], range: Range) {
    if (users.length <= 0) return undefined;

    const chunks = chunk(shuffle(users), range.lower);

    if (chunks[chunks.length - 1].length < range.lower) {
      const c = chunks.pop();
      let i = 0;
      while (c.length > 0) {
        chunks[i].push(c.pop());
        i = (i + 1) % chunks.length;
      }
    }

    return chunks;
  }

  public static mbti(users: IUser[], range: Range) {
    const chunks = this.random(users, range);

    if (chunks.length === 1) return chunks;

    const ATTEMPTS = 100000;
    let score = 0;

    for (let i = 0; i < ATTEMPTS; i++) {
      const swap = this.randomSwap(chunks);
      const tempScore = this.calculateMbtiScore(chunks);
      if (tempScore >= score) score = tempScore;
      else swap.cancel();
    }

    console.log("SCORE: " + score);
    return chunks;
  }

  private static calculateMbtiScore(chunks: IUser[][]) {
    return mean(chunks.map(team => this.mbtiTeamScore(team)));
  }

  private static mbtiTeamScore(team: IUser[]) {
    team.forEach(u => {
      if (!u.mbtiPoints) {
        u.mbtiPoints = ObjectUtils.initStrict(MBTI_PERSONALITY, 0);
      }
    });

    const scores = team.reduce<Quadruplet<number>>(
      (pre, u) => {
        const avgs = MBTI_HELPER.avg(u.mbtiPoints, "method1");
        pre = pre.map((v, i) => v + avgs[i]) as Quadruplet<number>;
        return pre;
      },
      [0, 0, 0, 0]
    );

    return Math.max(...scores);
  }

  private static randomSwap(chunks: IUser[][]) {
    if (chunks.length === 1) return;

    const c1 = RandomUtils.anyI(chunks);
    let c2: number;

    do {
      c2 = RandomUtils.anyI(chunks);
    } while (c1 === c2);

    const i1 = RandomUtils.anyI(chunks[c1]);
    const i2 = RandomUtils.anyI(chunks[c2]);

    [chunks[c1][i1], chunks[c2][i2]] = [chunks[c2][i2], chunks[c1][i1]];

    return {
      cancel: () =>
        ([chunks[c1][i1], chunks[c2][i2]] = [chunks[c2][i2], chunks[c1][i1]]),
    };
  }
}
