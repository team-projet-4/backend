import {
  ISuggestionForm,
  SuggestionAlgorithm,
  IUserPublicInfo,
} from "polyteam-shared";
import { User, IUser } from "../models/user.model";
import { Course, ICourseDocument } from "../models/course.model";
import { StrictMap } from "../utils/type.utils";
import { RandomUtils } from "../utils/random.utils";
import { ObjectUtils } from "../utils/object.utils";
import { MBTI_PERSONALITY } from "../config/mbti";
import { MBTI_HELPER } from "./mbti.service";
import { ArrayUtils } from "../utils/array.utils";

export class SuggestionService {
  public static async run(email: string, form: ISuggestionForm) {
    const res = await this.prepare(email, form.acronym);
    const { algorithm, user, users, course, allUsers } = res;
    const sorted = await ALGORITHMS[algorithm](user, users);

    return res ? this.finalize(email, sorted, course, allUsers) : [];
  }

  private static async prepare(email: string, courseId: string) {
    const user = await User.findOne({ email }).exec();
    const course = await Course.findOne({ courseId }).exec();

    if (!user || !course) return undefined;

    const likedUsers = user.likedUsers[courseId] || [];
    const emails = course.users.filter(u => u !== user.email);

    const promises = emails.map(e => User.findOne({ email: e }).exec());
    const allUsers = await Promise.all(promises);

    const filter = likedUsers.map(v => v.email);
    const users = allUsers.filter(u => !filter.includes(u.email));

    const algorithm = course.settings.algorithm;

    return { user, users, algorithm, course, allUsers };
  }

  private static finalize(
    email: string,
    users: IUser[],
    course: ICourseDocument,
    all: IUser[]
  ) {
    const emailTeams = users.map(u =>
      course.teams
        .find(t => t.users.find(tu => tu.email === u.email))
        .users.map(cu => cu.email)
    );

    ArrayUtils.removeAll(emailTeams, e => e.includes(email));

    const teams = emailTeams.map(emails =>
      emails.map(e =>
        ObjectUtils.pick(IUserPublicInfo, all.find(u => u.email === e))
      )
    );

    return teams;
  }
}

class Algorithms {
  public static async ordered(_: IUser, users: IUser[]): Promise<IUser[]> {
    return users;
  }

  public static async random(_: IUser, users: IUser[]): Promise<IUser[]> {
    return RandomUtils.shuffle(users);
  }

  public static async mbti(user: IUser, users: IUser[]): Promise<IUser[]> {
    if (!user.mbtiType) return await this.random(user, users);

    users.forEach(u => {
      if (!u.mbtiPoints) {
        u.mbtiPoints = ObjectUtils.initStrict(MBTI_PERSONALITY, 0);
      }
    });

    const match = MBTI_HELPER.get(user.mbtiType);
    const points = users.map(u => MBTI_HELPER.sum(u.mbtiPoints, match));
    const sort = (a: number, b: number) => b - a;

    return ArrayUtils.combineSort(users, points, sort);
  }
}

type Algorithm = (user: IUser, users: IUser[]) => Promise<IUser[]>;

const ALGORITHMS: StrictMap<SuggestionAlgorithm, Algorithm> = {
  RANDOM: Algorithms.random,
  MBTI: Algorithms.mbti,
  ORDERED: Algorithms.ordered,
};
