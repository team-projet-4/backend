import * as uniquid from "uniqid";
import { Code } from "../models/code.model";
import { User } from "../models/user.model";
import { Course } from "../models/course.model";

const CodeStatus = Promise;
type CodeStatus = Promise<[string, number]>;

export class CodeManagerService {
  public static async createCode(creator: string, course: string): CodeStatus {
    const user = await User.findOne({ email: creator }).exec();
    if (!user) return ["L'utilisateur n'a pas été trouvé.", 404];

    const dbcourse = await Course.findOne({ courseId: course }).exec();
    if (!dbcourse) return ["Le cours n'a pas été trouvé.", 404];

    const codes = await Code.find({ course }).exec();
    if (codes.some(c => c.members.includes(creator)))
      return ["Un token existe déja pour votre équipe.", 409];

    const members = dbcourse.teams
      .find(t => t.users.find(u => u.email === creator))
      .users.map(u => u.email);

    const code = uniquid();
    const dbCode = new Code({ code, creator, course, members });
    await dbCode.save();

    const users = await Promise.all(
      members.map(email => User.findOne({ email }).exec())
    );

    users.forEach(u => (u.codes[course] = code));
    await Promise.all(
      users.map(u => User.updateOne({ email: u.email }, u).exec())
    );

    return ["", 200];
  }

  public static async joinCode(email: string, code: string): CodeStatus {
    const user = await User.findOne({ email }).exec();
    if (!user) return ["L'utilisateur n'a pas été trouvé.", 404];

    const dbcode = await Code.findOne({ code }).exec();
    if (!dbcode) return ["Le code n'a pas été trouvé.", 404];

    const courseId = dbcode.course;
    if (user.codes[courseId] || dbcode.members.includes(email))
      return ["Un token existe déja pour votre équipe.", 404];

    const course = await Course.findOne({ courseId }).exec();
    if (!course) return ["Le cours n'a pas été trouvé.", 404];

    if (!course.users.includes(email))
      return ["L'utilisateur ne fait pas parti du cours", 404];

    const newTeam = course.teams.find(t =>
      t.users.find(u => u.email === dbcode.creator)
    );

    if (newTeam.users.length >= course.settings.nbUsers.upper)
      return ["Nombre d'utilisateurs maximum atteint pour cette équipe.", 401];

    const oldTeamIdx = course.teams.findIndex(t =>
      t.users.find(u => u.email === email)
    );

    const oldTeam = course.teams[oldTeamIdx];
    const userIdx = oldTeam.users.findIndex(u => u.email === email);
    if (userIdx !== -1) oldTeam.users.splice(userIdx, 1);
    if (oldTeam.users.length === 0) course.teams.splice(oldTeamIdx, 1);

    const { fname, lname } = user;
    newTeam.users.push({ email, fname, lname, validated: false });
    await Course.updateOne({ courseId }, course).exec();

    dbcode.members.push(email);
    user.codes[courseId] = code;

    await Code.updateOne({ code }, dbcode).exec();
    await User.updateOne({ email }, user).exec();

    return ["", 200];
  }
}
