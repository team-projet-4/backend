import {
  ILoginForm,
  IServerResponse as IResponse,
  LoginType,
  IUserInfo,
  IUserInfoCourse,
} from "polyteam-shared";
import { UserManagerService } from "./userManager.service";
import * as bcrypt from "bcrypt";
import * as jw from "jsonwebtoken";
import { CourseManagerService } from "./courseManager.service";
import { IUser, User } from "../models/user.model";

type TokenForm = { token: string; type: LoginType };
type AuthResponse = IResponse<IUserInfo>;

export class AuthService {
  public static async loginAuth(
    form: ILoginForm,
    reset = true
  ): Promise<AuthResponse> {
    const user = await UserManagerService.get(form);

    if (!user) return { code: 404, msg: "L'utilisateur n'a pas été trouvé." };

    if (!(await bcrypt.compare(form.password, user.password)))
      return {
        code: 401,
        msg: "Le mot de passe et le email ne corresponde pas.",
      };

    // TODO: ALLOW ADMIN TO BE TEACHER?

    if (form.loginType !== user.type)
      return {
        code: 401,
        msg: "L'utilisateur n'a pas le bon type.",
      };

    if (reset) {
      user.jwt = UserManagerService.createToken(form.email, form.password);
      await user.save();
    }

    const {
      jwt,
      email,
      fname,
      lname,
      profilepic,
      mbtiType: personality,
      type: logintype,
      codes,
    } = user;

    const courseDocuments = await CourseManagerService.match(
      email,
      form.loginType
    );

    let completedprofiles: number[] = [];

    if (logintype === "TEACHER") {
      const courseUsers = await Promise.all(
        courseDocuments.map(c =>
          User.find()
            .where("email")
            .in(c.users)
            .exec()
        )
      );

      completedprofiles = courseUsers.map(users => {
        const completed = users.filter(u => u.mbtiType).length;
        return (completed / users.length) * 100;
      });
    }

    const courses: IUserInfoCourse[] = courseDocuments.map((c, i) => ({
      courseId: c.courseId,
      locked: c.settings.creationType === "AUTO",
      completedProfiles: completedprofiles[i],
    }));

    const data = {
      jwt,
      email,
      courses,
      fname,
      lname,
      profilepic,
      personality,
      logintype,
      codes,
    };

    return { code: 200, msg: "L'utilisateur est connecté!", data };
  }

  public static async tokenAuth(form: TokenForm) {
    const user = await UserManagerService.get(form.token);

    if (!user) return { code: 404, msg: "L'utilisateur n'a pas été trouvé." };

    let decoded: { email: string; password: string };

    try {
      decoded = jw.verify(form.token, process.env.APP_JWT_SECRET) as any;
    } catch (e) {
      return {
        code: 401,
        msg:
          e.message === "jwt expired"
            ? "Le token est expiré"
            : "Le token n'est pas valide.",
      };
    }

    if (!decoded.email || !decoded.password)
      return {
        code: 401,
        msg: "Le token n'est pas valide.",
      };

    return this.loginAuth(
      {
        email: decoded.email,
        password: decoded.password,
        loginType: form.type,
      },
      false
    );
  }
}
