import * as NodeMailer from "nodemailer";

export class MailService {
  private static instance: MailService;
  private transporter: NodeMailer.Transporter;

  public static getInstance() {
    if (!this.instance) {
      this.instance = new MailService();
    }

    return this.instance;
  }

  private constructor() {
    this.transporter = NodeMailer.createTransport({
      host: process.env.APP_MAILER_HOST,
      port: 465,
      secure: true,
      auth: {
        user: process.env.APP_MAILER_USERNAME,
        pass: process.env.APP_MAILER_PASS,
      },
    });
  }

  public static async send(to: string, subject: string, text: string) {
    return this.getInstance().send(to, subject, text);
  }

  public async send(to: string, subject: string, text: string) {
    let info: any;

    try {
      info = await this.transporter.sendMail({
        from: "no-reply@polyteam.ca",
        to,
        subject,
        text,
      });
    } catch (e) {
      // tslint:disable-next-line:no-console
      console.log(e);
    }

    return info;
  }
}
