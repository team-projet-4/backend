import { XOR } from "utils/type.utils";
import { User, IUserDocument } from "../models/user.model";
import {
  LoginType,
  ICreateAccount,
  PersonalityType,
  IUpdateAccount,
  IUserPublicInfo,
  IUserPersonalInfo,
} from "polyteam-shared";
import * as bcrypt from "bcrypt";
import * as generator from "generate-password";
import * as jw from "jsonwebtoken";
import { MailService } from "./mail.service";
import { MBTIPoints } from "../config/mbti";
import { ObjectUtils } from "../utils/object.utils";

type IGetUser = { email: string; password?: string };

const SALT_ROUNDS = 10;

export class UserManagerService {
  public static async listAllUsers() {
    return (await User.find()).map(u => ObjectUtils.pick(IUserPersonalInfo, u));
  }

  public static async createDefaultAdmin() {
    const email = process.env.APP_DEFAULT_ADMIN;
    const fname = email;
    const lname = email;

    if (await User.findOne({ email }))
      return console.log("Admin User Already Exists");

    console.log("Creating Admin User");

    const raw = process.env.APP_DEFAULT_ADMIN_PASS;
    const password = await this.encryptPassword(raw);
    const jwt = this.createToken(email, raw);
    const type: LoginType = "ADMIN";

    const admin = new User({
      email,
      password,
      jwt,
      type,
      fname,
      lname,
    });

    await admin.save();
  }

  public static async create(form: ICreateAccount, type: LoginType) {
    const { email, fname, lname, profilepic } = form;

    const u = await User.findOne({ email }).exec();
    if (u) return [false, u.type] as [boolean, LoginType];

    const raw = this.createPassword();
    const password = await this.encryptPassword(raw);
    const jwt = this.createToken(email, raw);

    const user = new User({
      email,
      password,
      jwt,
      type,
      fname,
      lname,
      profilepic,
    });

    const text = "Votre mot de passe temporaire est " + raw;

    try {
      await user.save();
      await MailService.send(email, "Création de votre compte Polyteam", text);
      return [true, user.type] as [boolean, LoginType];
    } catch (e) {
      console.log(e);
      return [false, undefined];
    }
  }

  public static async get(input: XOR<string, IGetUser>) {
    let user: IUserDocument;

    if (typeof input === "string") {
      // INPUT = JWT
      user = await User.findOne({ jwt: input }).exec();
    } else {
      user = await User.findOne({ email: input.email }).exec();
    }

    return user;
  }

  public static async setMBTI(
    email: string,
    pts: MBTIPoints,
    type: PersonalityType
  ) {
    const user = await User.findOne({ email }).exec();
    if (!user) return false;

    user.mbtiType = type;
    user.mbtiPoints = pts;

    await user.save();

    return true;
  }

  public static async update(email: string, update: IUpdateAccount) {
    const user = await User.findOne({ email }).exec();
    if (!user) return undefined;

    Object.keys(update).forEach(k => (user[k] = update[k]));

    if (update.password) {
      const password = await this.encryptPassword(update.password);
      const jwt = this.createToken(email, update.password);
      user.password = password;
      user.jwt = jwt;
    }

    await user.save();

    return user.jwt;
  }

  public static delete() {
    // TODO: Implement
  }

  private static createPassword() {
    return generator.generate({ length: 8, numbers: true, uppercase: true });
  }

  private static async encryptPassword(password: string) {
    return await bcrypt.hash(password, SALT_ROUNDS);
  }

  public static createToken(email: string, password: string): string {
    return jw.sign({ email, password }, process.env.APP_JWT_SECRET, {
      expiresIn: "30d",
    });
  }
}
