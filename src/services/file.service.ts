import * as fs from "fs";
import { UploadedFile } from "express-fileupload";
import * as uniqid from "uniqid";

export class FileService {
  public static save(file: UploadedFile) {
    const id = uniqid();
    const ext = file.name.split(".").pop();
    const filename = id + "." + ext;
    const path = `src/public/${filename}`;
    fs.writeFileSync(path, file.data);

    return filename;
  }
}
