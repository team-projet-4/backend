import { Course, ICourse } from "../models/course.model";
import {
  ICreateUserGroup,
  LoginType,
  IUserGroupSettings,
  ICreateAccount,
} from "polyteam-shared";
import { UserManagerService } from "./userManager.service";

export class CourseManagerService {
  public static async createOrUpdate(
    form: ICreateUserGroup,
    teacher: string
  ): Promise<[number, string]> {
    let users = form.users;
    const course = await this.get(form.acronym);

    if (course && course.teacher !== teacher) {
      return [401, "Ce cours est assigné à un autre professeur."];
    }

    const statuses = await Promise.all(
      form.users.map(u => UserManagerService.create(u, "USER"))
    );

    users = users.filter((_, i) => statuses[i][1] && statuses[i][1] === "USER");

    if (!course) {
      await this.create(form.acronym, users, teacher, form.settings);
      return [200, undefined];
    }

    // UPDATE TEAMS
    const newUsers = users.filter(u => !course.users.includes(u.email));
    const oldUsers = course.users.filter(o => !users.find(u => u.email === o));

    // Update teams with remove member
    course.teams.forEach(t => {
      const len = t.users.length;
      t.users = t.users.filter(u => !oldUsers.includes(u.email));
      if (len !== t.users.length) {
        t.ready = false;
        t.users.forEach(u => (u.validated = false));
      }
    });

    // Remove empty teams
    course.teams = course.teams.filter(t => t.users.length > 0);

    const ready = false;
    const validated = false;

    // Add new users in empty teams
    course.teams = [
      ...course.teams,
      ...newUsers.map(({ email, fname, lname }) => ({
        users: [{ email, fname, lname, validated }],
        ready,
      })),
    ];

    course.users = users.map(u => u.email);
    await Course.updateOne({ courseId: form.acronym }, course).exec();

    return [200, undefined];
  }

  public static async get(courseId: string) {
    return await Course.findOne({ courseId }).exec();
  }

  public static async match(email: string, type: LoginType) {
    if (type === "ADMIN") return [];

    const courses = await Course.find().exec();

    if (type === "USER") return courses.filter(c => c.users.includes(email));
    if (type === "TEACHER") return courses.filter(c => c.teacher === email);
  }

  public static async create(
    id: string,
    users: ICreateAccount[],
    teacher: string,
    settings: IUserGroupSettings
  ) {
    const course = new Course(new ICourse(id, users, teacher, settings));
    await course.save();
  }
}
