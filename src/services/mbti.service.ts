import {
  MBTI_FORM,
  MBTIPoints,
  MBTI_PERSONALITY,
  MBTI_VALIDATOR,
  MBTIPersonality,
} from "../config/mbti";
import { MBTIForm, MBTIAnswerForm, PersonalityType } from "polyteam-shared";
import { ObjectUtils } from "../utils/object.utils";
import { UserManagerService } from "./userManager.service";
import { sum, mean } from "lodash";
import { Map, StrictMap, Quadruplet } from "../utils/type.utils";

interface IProcessedAnswers {
  points: MBTIPoints;
  type: PersonalityType;
}

export class MBTIService {
  public static getMBTIForm(): MBTIForm {
    return MBTI_FORM;
  }

  public static async submitForm(
    email: string,
    form: MBTIAnswerForm
  ): Promise<PersonalityType | undefined> {
    const processed = this.processMBTIAnswers(form);

    console.log(processed);

    const res = await UserManagerService.setMBTI(
      email,
      processed.points,
      processed.type
    );

    return res ? processed.type : undefined;
  }

  private static processMBTIAnswers(form: MBTIAnswerForm): IProcessedAnswers {
    const points: MBTIPoints = ObjectUtils.initStrict(MBTI_PERSONALITY, 0);

    console.log(points);

    form.forEach((part, pi) =>
      part.answers.forEach(
        (ans, ai) => points[MBTI_VALIDATOR(pi, ai, ans.answer)]++
      )
    );

    const check = this.pointsChecker(points);
    const type: PersonalityType = [
      check("E", "I"),
      check("S", "N"),
      check("T", "F"),
      check("J", "P"),
    ];

    return { points, type };
  }

  private static pointsChecker(points: MBTIPoints) {
    return <T extends MBTIPersonality>(a: T, b: T) => {
      return points[a] > points[b] ? a : b;
    };
  }
}

type Method = "method1";

class MBTIHelper {
  // Source: https://www.bustle.com/p/which-myers-briggs-types-work-best-together-these-pairs-will-always-get-along-8510988

  private matches: Map<PersonalityType> = {
    ENFP: ["I", "N", "F", "J"],
    INFP: ["E", "N", "F", "J"],
    INTJ: ["E", "S", "T", "J"],
    INTP: ["E", "N", "T", "P"],
    ISFP: ["E", "S", "F", "P"],
    ISFJ: ["I", "S", "F", "J"],
    ISTJ: ["E", "S", "T", "P"],
    ISTP: ["E", "N", "T", "J"],
    INFJ: ["E", "N", "F", "P"],
    ENFJ: ["I", "N", "F", "P"],
    ESTJ: ["I", "N", "T", "J"],
    ENTP: ["I", "N", "T", "P"],
    ESFP: ["I", "S", "F", "P"],
    ESTP: ["I", "S", "T", "J"],
    ENTJ: ["I", "S", "T", "P"],
  };

  // tslint:disable-next-line:max-line-length
  // Source: https://thoughtcatalog.com/heidi-priebe/2017/02/8-ways-of-grouping-myers-briggs-types-that-would-make-more-sense-than-keirseys-temperament-groups/

  private groups: StrictMap<Method, Quadruplet<PersonalityType>[]> = {
    method1: [
      [
        ["I", "N", "T", "J"],
        ["E", "N", "T", "J"],
        ["I", "N", "T", "P"],
        ["E", "N", "T", "P"],
      ],
      [
        ["I", "N", "F", "J"],
        ["E", "N", "F", "J"],
        ["I", "N", "F", "P"],
        ["E", "N", "F", "P"],
      ],
      [
        ["I", "S", "T", "J"],
        ["E", "S", "T", "J"],
        ["I", "S", "T", "P"],
        ["E", "S", "T", "P"],
      ],
      [
        ["I", "S", "F", "J"],
        ["E", "S", "F", "J"],
        ["I", "S", "F", "P"],
        ["E", "S", "F", "P"],
      ],
    ],

    // 7 more methods available
  };

  public get(type: PersonalityType): PersonalityType {
    return this.matches[type.join("")];
  }

  public sum(points: MBTIPoints, type: PersonalityType) {
    return sum(type.map(t => points[t]));
  }

  public avg(points: MBTIPoints, method: Method) {
    return this.groups[method].map(p =>
      mean(p.map(v => this.sum(points, v)))
    ) as Quadruplet<number>;
  }
}

export const MBTI_HELPER = new MBTIHelper();
