import { Code } from "../models/code.model";
import { Course } from "../models/course.model";
import { User, IUser } from "../models/user.model";
import { Backup } from "../models/backup.model";

export class BackupService {
  public static async reset(key: string) {
    const codes = await Code.find().exec();
    const courses = await Course.find().exec();
    const users = await User.find().exec();

    const backup = new Backup({
      key,
      backup: { codes, courses, users },
    });

    try {
      await backup.save();
      await this.deleteAll();
      return true;
    } catch (e) {
      return false;
    }
  }

  private static async deleteAll() {
    await Code.deleteMany({}).exec();
    await Course.deleteMany({}).exec();

    const users = await User.find().exec();

    users.forEach(u => this.resetUser(u));

    await Promise.all(users.map(u => User.updateOne({ email: u.email }, u)));
  }

  private static resetUser(user: IUser) {
    user.codes = {};
    user.likedUsers = {};
  }
}
