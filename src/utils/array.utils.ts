export class ArrayUtils {
  public static combineSort<T, U>(
    ta: T[],
    ua: U[],
    cmp?: (a: U, b: U) => number
  ) {
    const merged = ta.map((t, i) => ({
      t,
      u: ua[i],
    }));

    merged.sort((a, b) => cmp(a.u, b.u));

    return merged.map(m => m.t);
  }

  public static allIndexes<T>(arr: T[], f: (o: T) => boolean) {
    const indexes: number[] = [];
    for (let i = 0; i < arr.length; i++) if (f(arr[i])) indexes.push(i);

    return indexes;
  }

  public static removeAll<T>(arr: T[], f: (o: T) => boolean) {
    const indexes = this.allIndexes(arr, f);
    indexes.forEach(i => arr.splice(i, 1));
  }
}
