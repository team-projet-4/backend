import { Express } from "express";
import { ROUTES as R } from "../controllers/route.manager";

import {
  adminGuard,
  teacherGuard,
  userGuard,
  authController,
  anyGuard,
} from "../controllers/auth.controller";

import { userController } from "../controllers/user.controller";
import { mbtiController } from "../controllers/mbti.controller";
import { groupController } from "../controllers/group.controller";
import { indexController } from "../controllers/index.controller";
import { uploadController } from "../controllers/upload.controller";

export default class Routes {
  constructor(app: Express) {
    app.route("/").get(indexController.index);
    app.route("/msg").get(indexController.msg);

    // AUTH
    R.login.register(app, authController.login);
    R.token.register(app, authController.token);

    // MBTI
    R.mbtiForm.register(app, mbtiController.getForm);
    R.mbtiProcess.register(app, mbtiController.process, userGuard);

    // USER MANAGEMENT
    R.admin.register(app, userController.createAdmin, adminGuard);
    R.teacher.register(app, userController.createTeacher, adminGuard);
    R.teacherList.register(app, userController.uploadList, teacherGuard);
    R.updateUser.register(app, userController.updateUser, anyGuard);

    // USER SUGGESTION / TEAMS
    R.suggestion.register(app, groupController.suggestion, userGuard);
    R.likeUser.register(app, groupController.likeUser, userGuard);
    R.getTeam.register(app, groupController.getTeam, userGuard);
    R.getTeams.register(app, groupController.getTeams, teacherGuard);
    R.updateTeams.register(app, groupController.updateTeams, teacherGuard);
    R.createTeams.register(app, groupController.createTeams, teacherGuard);
    R.createCode.register(app, groupController.createCode, userGuard);
    R.joinCode.register(app, groupController.joinCode, userGuard);
    R.validateTeam.register(app, groupController.validateTeam, userGuard);

    // File Upload
    R.upload.register(app, uploadController.uploadFile, anyGuard);

    // Admin Operations
    R.listAllUsers.register(app, userController.listAllUsers, adminGuard);
    R.reset.register(app, userController.reset, adminGuard);
  }
}
